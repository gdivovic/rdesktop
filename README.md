This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm intall` for installing the dependencies
### `npm start` for running the app

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

## Connecting environment with Contentful
1. Duplicate `.env.example` and rename it to `.env`
2. From Contentful generate `CONTENTFUL_SPACE_ID` and `CONTENTFUL_DELIVERY_TOKEN` keys and add them to `.env` file

## Deployment
Right now, automatic deplyment is defined on Netlify on each new commit which arrive to master.
These configurations can be changed on Netlify's website

## Used tools
- Text copy: [Contentful](https://www.contentful.com)
- Deployment and form submissions: [Netlify](https://netlify.com)

### Author
Danijel Grabež  
website: [www.danijelgrabez.com](www.danijelgrabez.com)  
email: [danijel889@gmail.com](mailto:danijel889@gmail.com)  
