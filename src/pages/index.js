export { default as Home } from './Home';
export { default as NotFound } from './NotFound';
export { default as Privacy } from './Privacy';
export { default as Success } from './Success';
export { default as Terms } from './Terms';
