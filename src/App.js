import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Home, Terms, Privacy, NotFound, Success } from './pages';

const App = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={Home} />} />
      <Route exact path="/terms-of-services" component={Terms} />} />
      <Route exact path="/privacy-policy" component={Privacy} />} />
      <Route exact path="/success" component={Success} />} />
      <Route component={NotFound} />
    </Switch>
  </BrowserRouter>
);

export default App;
