import * as React from 'react';
import PropTypes from 'prop-types';
import get from 'lodash.get';
import { Container, Image } from '../../components';
import {
  Wrapper,
  Title,
  Text,
  Figure,
  Content,
  Header,
} from './Testimonials.styles';

/**
 * Testimonials section
 */
const Testimonials = ({ data }) => {
  const imageUrl = data && get(data.image.fields.file, 'url');

  return data ? (
    <Wrapper background="neutral">
      <Container kind="narrow" justifyContent="flex-start">
        <Content>
          <Figure>
            <Image
              src={imageUrl}
              alt="Testimonial from our client"
              size={110}
            />
          </Figure>
          <Header>
            <Title element="h3" size="h5">
              {data.title}
            </Title>
            <Text size="large">{data.testimonial}</Text>
          </Header>
        </Content>
      </Container>
    </Wrapper>
  ) : null;
};

Testimonials.propTypes = {
  /**
   * Data passed from Contenetful
   */
  data: PropTypes.object,
};

Testimonials.defaultProps = {
  data: null,
};

export default Testimonials;
