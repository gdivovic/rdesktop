import * as React from 'react';
import PropTypes from 'prop-types';
import { Heading, Column, Image } from '../../components';
import { Wrapper, Feature, Figure, FeatureText } from './KeyFeatures.styles';
import featureOne from '../../assets/images/apache-guacamole.png';
import featureTwo from '../../assets/images/browsers.svg';
import featureThree from '../../assets/images/lock.svg';

// const regexBold = /\_\_(\S(.*?\S)?)\_\_/gm;
// const regexItalic = /\_(\S(.*?\S)?)\_/gm;
// const parseBoldMarkdown = (str: string) => str.replace(regexBold, '<strong>$1</strong>');

/**
 * Key Features section
 */
const KeyFeatures = ({ data }) => {
  return data ? (
    <Wrapper background="neutral">
      <Column span={[12, 12, 4]}>
        <Feature>
          <Figure>
            <Image
              src={featureOne}
              alt="R Desktop is powered by Apache Guacamole"
              size={72}
            />
          </Figure>
          <Heading element="h3" size="h4">
            {data.featureOneTitle}
          </Heading>
          <FeatureText>{data.featureOneDescription}</FeatureText>
        </Feature>
      </Column>
      <Column span={[12, 12, 4]}>
        <Feature>
          <Figure>
            <Image
              src={featureTwo}
              alt="R Desktop only needs browser to be used, no plugins are required"
              size={72}
            />
          </Figure>
          <Heading element="h3" size="h4">
            {data.featureTwoTitle}
          </Heading>
          <FeatureText>{data.featureTwoDescription}</FeatureText>
        </Feature>
      </Column>
      <Column span={[12, 12, 4]}>
        <Feature>
          <Figure>
            <Image
              src={featureThree}
              alt="R Desktop provides secure, centralized access"
              size={54}
            />
          </Figure>
          <Heading element="h3" size="h4">
            {data.featureThreeTitle}
          </Heading>
          <FeatureText>{data.featureThreeDescription}</FeatureText>
        </Feature>
      </Column>
    </Wrapper>
  ) : null;
};

KeyFeatures.propTypes = {
  /**
   * Data passed from Contenetful
   */
  data: PropTypes.object,
};

KeyFeatures.defaultProps = {
  data: null,
};

export default KeyFeatures;
