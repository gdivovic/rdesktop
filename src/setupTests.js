import React from 'react';
import { configure, shallow, render, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import * as emotion from 'emotion';
import { createSerializer } from 'jest-emotion';
import { ThemeProvider } from 'emotion-theming';
import { theme } from './common/theme';
import { create } from 'react-test-renderer';

configure({ adapter: new Adapter() });

expect.addSnapshotSerializer(createSerializer(emotion));

// Configure global stuff
const renderWithTheme = renderFn => (component, ...rest) =>
  renderFn(<ThemeProvider theme={theme}>{component}</ThemeProvider>, rest);

const shallowWithTheme = tree => {
  const context = shallow(<ThemeProvider theme={theme} />)
    .instance()
    .getChildContext();

  return shallow(tree, { context });
};

const mountWithTheme = tree => {
  const context = shallow(<ThemeProvider theme={theme} />)
    .instance()
    .getChildContext();

  return mount(tree, {
    context,
    childContextTypes: ThemeProvider.childContextTypes,
  });
};

global.shallow = shallowWithTheme;
global.render = renderWithTheme(render);
global.create = renderWithTheme(create);
global.mount = mountWithTheme;
